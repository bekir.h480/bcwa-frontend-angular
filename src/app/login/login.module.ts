import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from "../core/core.module";

import { LoginComponent } from "./components/pages/login/login.component";
import { FormsModule } from "@angular/forms";
import {RouterOutlet} from "@angular/router";


@NgModule({
  declarations: [
    LoginComponent
  ],
  exports: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    RouterOutlet
  ]
})
export class LoginModule { }
